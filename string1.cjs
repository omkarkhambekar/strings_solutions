function string1(string){
    //removing any non digit characters using .replace regex method 
    const requiredString = string.replace(/[^\d.-]/g,'');
    //this will convert requiredString into floating numeric form
    const numericValue = parseFloat(requiredString);

    //this check will check if given numeric value is number or not 
    if(Number.isNaN(numericValue)){
        return 0;
    }
    return numericValue;
}
module.exports = string1;