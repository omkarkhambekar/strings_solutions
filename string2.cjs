function string2(string){
    //splitting the given string by dot 
    const splitted = string.split('.');
    //for storing the string content in array form
    const ipv4Array = [];
    for(let index = 0; index < splitted.length; index++){
        const parts = splitted[index]; 
        if(isNaN(parts) || parts < 0 || parts > 255 ){
            return [];
        }
        if(parts[0]=='0' && parts.length !== 1){
            return [];
        }
        ipv4Array.push(parts);
    }
    return ipv4Array;
}
module.exports = string2;