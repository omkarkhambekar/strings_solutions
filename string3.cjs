function string3(string){
    const splitted = string.split('/');
    const monthsName = ["January","February","March","April","May","June",
    "July","August","September","November","December"];
    const monthNumber = parseInt(splitted[1]);
    const monthName = monthsName[monthNumber-1];
    return monthName;
}
module.exports = string3;