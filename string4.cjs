function string4(obj){
    //taking objects key values for first name and converting first char to capital and rest is lower 
    const firstName = obj.first_name.charAt(0).toUpperCase()+obj.first_name.slice(1).toLowerCase();
    //if someone has middle name we are checking here
    let middleName = '';
    if(obj.middle_name){
        middleName = obj.middle_name.charAt(0).toUpperCase()+obj.middle_name.slice(1).toLowerCase();
    }
    //for last name
    const lastName = obj.last_name.charAt(0).toUpperCase()+obj.last_name.slice(1).toLowerCase();
    return `${firstName} ${middleName} ${lastName}`.trim();
}
module.exports=string4;