function string5(givenArray){
    //to grab the string from array
    let requiredString = '';
    //edge case if array is empty
    if(givenArray.length === 0){
        return requiredString;
    }
    //this will concatenate the every string one by one from the array and store it
    for(let index = 0; index < givenArray.length; index++){
        requiredString = requiredString.concat(givenArray[index] + ' ');
    }
    return requiredString + ' ';
}
module.exports = string5;